//
//  CenterTextView.swift
//  Eget Projekt
//
//  Created by Christian on 2016-04-25.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

import UIKit

class CustomTextView: UITextView {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.New, context: nil)
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        let textView:UITextView = object as! UITextView
        let topCorrect = (textView.frame.size.height - textView.contentSize.height)*textView.zoomScale/2.0
        textView.contentInset = UIEdgeInsetsMake(topCorrect, 0, 0, 0)
    }

}
