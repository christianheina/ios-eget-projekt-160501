//
//  DataManager.swift
//  Eget Projekt
//
//  Created by Christian on 2016-04-23.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

import UIKit
import CoreData
import Darwin

class DataManager: NSObject {
    var appDelegate: AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    var context: NSManagedObjectContext!
    var userDefault = NSUserDefaults.standardUserDefaults()
    var nBackgroundImages:UInt32 = 7
    
    override init() {
        super.init()
        context = appDelegate.managedObjectContext
    }
    
    func addScore (score: Int, date: NSDate) {
        let newScore = NSEntityDescription.insertNewObjectForEntityForName("Score", inManagedObjectContext: context) as NSManagedObject
        newScore.setValue(score, forKey: "score")
        newScore.setValue(date, forKey: "date")
        do {
            try self.context.save()
        } catch let error as NSError {
            print(error)
        }
    }
    
    func getScores () -> [Score] {
        var scores: [Score] = []
        let request = NSFetchRequest(entityName: "Score")
        do {
            scores = try context.executeFetchRequest(request) as! [Score]
        } catch let error as NSError {
            print(error)
        }
        return scores
    }
    
    func getSortedScores () -> [Score] {
        let array = getScores()
        return array.sort({ (first, second) -> Bool in
            return Int(first.score!) > Int(second.score!)
        })
    }
    
    func getStoryCompletedForName(name: String) -> Bool {
        var stories: [Stories] = []
        let request = NSFetchRequest(entityName: "Stories")
        request.predicate = NSPredicate(format: "name == %@", name)
        do {
            stories = try context.executeFetchRequest(request) as! [Stories]
        } catch let error as NSError {
            print(error)
        }
        if stories.count == 0 {
            return false
        }
        if let complete = stories[0].isCompleted {
            return complete.boolValue
        }
        return false
    }
    
    func saveStoryIfNotExists (name: String, completed: Bool) {
        var stories: [Stories] = []
        let request = NSFetchRequest(entityName: "Stories")
        request.predicate = NSPredicate(format: "name == %@", name)
        do {
            stories = try context.executeFetchRequest(request) as! [Stories]
        } catch let error as NSError {
            print(error)
        }
        if stories.count != 0 {
            return
        }
        let newScore = NSEntityDescription.insertNewObjectForEntityForName("Stories", inManagedObjectContext: context) as NSManagedObject
        newScore.setValue(name, forKey: "name")
        newScore.setValue(completed.hashValue, forKey: "isCompleted")
        do {
            try self.context.save()
        } catch let error as NSError {
            print(error)
        }
    }
    
    func getReverses () -> Int {
        let reverse = userDefault.integerForKey("reverses")
        return reverse
    }
    
    func shouldRewardAndAddReverse (score:Int) -> Bool {
        var score = score
        if score < 50 {
            score /= 3
        } else {
           score = Int(25 + pow(1.05, Double(score)))
            if score > 50 {
                score = 50
            }
        }
        let rand = Int(1 + arc4random()%100)
        if rand < score {
          setReverses(getReverses()+1)
            return true
        }
        return  false
    }
    
    func setReverses (reverses: Int) {
        userDefault.setInteger(reverses, forKey: "reverses")
        userDefault.synchronize()
    }
    
    func getVolume () -> Float {
        let volume = userDefault.floatForKey("backgroundVolume")
        return volume
    }
    
    func setVolume (volume:Float) {
        userDefault.setFloat(volume, forKey: "backgroundVolume")
        userDefault.synchronize()
    }
    
    
    func getLaunched () -> Bool {
        let launch = userDefault.boolForKey("launchedBefore")
        return launch
    }
    
    func launched () {
        userDefault.setBool(true, forKey: "launchedBefore")
        userDefault.synchronize()
    }
    
}
