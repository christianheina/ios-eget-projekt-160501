//
//  GameScene.swift
//  Eget Projekt
//
//  Created by Christian on 2016-04-12.
//  Copyright (c) 2016 Christian Heina. All rights reserved.
//

import SpriteKit
import Darwin
import CoreMotion

class GameScene: SKScene {
    var box = SKSpriteNode()
    var floor = SKSpriteNode()
    internal var motionManager: CMMotionManager = CMMotionManager()
    internal var destBox:CGPoint = CGPointMake(0, 0)
    var score:Int = 0
    internal var lastObjectTime:Double = 0
    var baseInterval:Double = 3.0
    var endGame:Bool = false
    var lives:Int = 1
    var goal:Int = -1
    var floorString = "floor1"
    
    override func didMoveToView(view: SKView) {
        floor = SKSpriteNode(imageNamed: floorString)
        floor.size = self.frame.size
        floor.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        floor.zPosition = -1
        floor.physicsBody?.collisionBitMask = 0
        self.addChild(floor)
        
        box = SKSpriteNode(imageNamed: "basket")
        box.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        box.zPosition = 0
        box.setScale(0.18)
        self.addChild(box)
        
        let xRange = SKRange(lowerLimit: 0, upperLimit: self.frame.width)
        let yRange = SKRange(lowerLimit: 0, upperLimit: self.frame.height)
        box.constraints = [SKConstraint.positionX(xRange, y: yRange)]
        
        destBox  = CGPointMake(box.position.x, box.position.y)
        startGyroUpdates()
        }
   
    override func update(currentTime: CFTimeInterval) {
        if lives > 0 && !(score >= goal && goal > 0) {
            gyroData()
            let boxAction = SKAction.moveTo(destBox, duration: 1)
            self.box.runAction(boxAction)
            let objectInterval:Double = timeBetweenDropForScore(self.score)
            if NSDate().timeIntervalSince1970 - self.lastObjectTime > objectInterval {
                createCircle()
                print(objectInterval)
            }
        } else if !endGame {
            gameOver()
            endGame = true
        }
    }
    
    func createCircle() {
        let circle = SKShapeNode(circleOfRadius: 50)
        circle.position = CGPointMake((CGFloat(10 + arc4random()) % (frame.width-20)), (CGFloat(10 + arc4random()) % (frame.height-20)))
        circle.strokeColor = SKColor.redColor()
        circle.glowWidth = 2.0
        circle.zPosition = 1
        self.addChild(circle)
        let dot = SKShapeNode(circleOfRadius: 5)
        dot.fillColor = SKColor.redColor()
        dot.strokeColor = SKColor.redColor()
        circle.addChild(dot)
        let removeCircle = SKAction.scaleTo(0.4, duration: self.baseInterval-1)
        circle.runAction(removeCircle, completion: {() -> Void in
            if CGRectContainsPoint(self.box.frame, circle.position) && !self.endGame {
                self.score += 1
                self.runAction(SKAction.playSoundFileNamed("catch", waitForCompletion: false))
            } else if !self.endGame {
                self.lives -= 1
                self.runAction(SKAction.playSoundFileNamed("break", waitForCompletion: false))
            }
            circle.hidden = true
            circle.removeFromParent()
        })
        let dotSize = SKAction.scaleTo(5, duration: self.baseInterval-1)
        dot.runAction(dotSize)
        self.lastObjectTime = NSDate().timeIntervalSince1970
    }
    
    func timeBetweenDropForScore (score: Int) -> Double {
        var objectInterval:Double = baseInterval - pow(1.03, Double(score))
        if objectInterval < 1.0 {
            objectInterval = 1.0
            objectInterval = objectInterval - (objectInterval * 0.005 * Double(score)/2)
            if objectInterval < 0.65 {
                objectInterval = 0.65
            }
            let everyHundred: Int = score/100
            objectInterval -= Double(everyHundred)/10
        }
        return objectInterval
    }
    
    func gameOver() {
        stopGyroUpdates()
        floor.zPosition = 2
        box.zPosition = 3
        let endBackground = SKShapeNode(rectOfSize: CGSize(width: frame.size.width, height: frame.size.height))
        endBackground.position = CGPointMake(frame.size.width/2, frame.size.height/2)
        endBackground.fillColor = SKColor.blackColor()
        endBackground.zPosition = 4
        endBackground.alpha = 0.8
        self.addChild(endBackground)
    }
    
    func startGyroUpdates() {
        if motionManager.gyroAvailable {
            self.motionManager.startGyroUpdates()
        }
    }
    
    func gyroData() {
        if let data = self.motionManager.gyroData {
            let newX = self.box.position.x - CGFloat(data.rotationRate.x) * 1000
            let newY = self.box.position.y - CGFloat(data.rotationRate.y) * 1000

            self.destBox = CGPointMake(newX, newY)
        }
    }
    
    func stopGyroUpdates() {
        if motionManager.gyroAvailable {
            motionManager.stopGyroUpdates()
        }
    }
}
