//
//  GameViewController.swift
//  Eget Projekt
//
//  Created by Christian on 2016-04-12.
//  Copyright (c) 2016 Christian Heina. All rights reserved.
//

import UIKit
import SpriteKit
import Social

class GameViewController: UIViewController {
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var livesLabel: UILabel!
    @IBOutlet weak var gameOverView: UIView!
    @IBOutlet weak var finalScoreLabel: UILabel!
    @IBOutlet weak var gameRewardDetails: UILabel!
    internal var skView: SKView!
    internal var timer: NSTimer!
    internal var scene: GameScene!
    var finalScore: Int = 0
    var lives:Int?
    var goal:Int?
    var objectiveInfo: String?
    var baseDropInterval:Double?
    var floor: String?
    var reverseUsed = false
    internal var storyMode = false
    internal var dataManager: DataManager = DataManager()
    @IBOutlet weak var reverseButtonOut: UIButton!
    @IBOutlet weak var shareButtonOut: UIButton!
    @IBOutlet weak var storyInfoText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newGame(0)
        navigationController?.navigationBarHidden = true
    }
    
    func letScreenRestIfNeeded() {
        UIApplication.sharedApplication().idleTimerDisabled = true
    }
    
    override func viewDidDisappear(animated: Bool) {
        UIApplication.sharedApplication().idleTimerDisabled = false
    }
    
    func uiUpdate () {
        if scene.endGame {
            gameOver()
        }
        livesLabel.text! = "Lives: \(scene.lives)"
        scoreLabel.text! = "Score: \(scene.score)"
    }
    
    func gameOver () {
        letScreenRestIfNeeded()
        self.gameRewardDetails.text = "No reward, better luck next time"
        finalScore = scene.score
        scoreLabel.hidden = true
        if storyMode {
            storyModeEndView()
        } else {
            freeModeEndView()
        }
        timer.invalidate()
        timer = nil
        gameOverView.hidden = false
    }
    
    func newGame (score: Int) {
        gameOverView.hidden = true
        livesLabel.hidden = true
        livesLabel.alpha = 0.8
        scoreLabel.hidden = false
        scoreLabel.alpha = 0.8
        scene = GameScene(fileNamed:"GameScene")
        scene.score = score
        
        if let lives = self.lives {
            scene.lives = lives
            print(lives)
            storyMode = true
            livesLabel.hidden = false
        }
        if let goal = self.goal {
            scene.goal = goal
            print(goal)
            storyMode = true
        }
        if let baseInterval = self.baseDropInterval {
            scene.baseInterval = baseInterval
            print(baseInterval)
            storyMode = true
        }
        if let floor = self.floor {
            scene.floorString = floor
        }
        
        // Configure the view.
        skView = self.view as! SKView
        //skView.showsFPS = true
        //skView.showsNodeCount = true

        /* Sprite Kit applies additional optimizations to improve rendering performance */
        skView.ignoresSiblingOrder = true
            
        /* Set the scale mode to scale to fit the window */
        scene.scaleMode = .AspectFill
            
        skView.presentScene(scene)
        timer = NSTimer.scheduledTimerWithTimeInterval(0.3, target: self, selector: Selector("uiUpdate"), userInfo: nil, repeats: true)
    }
    
    func storyModeEndView () {
        shareButtonOut.hidden = true
        reverseButtonOut.hidden = true
        if finalScore >= scene.goal {
            finalScoreLabel.text! = "Success! You completed the objective"
            if dataManager.getStoryCompletedForName(objectiveInfo!) {
                gameRewardDetails.text = "You have completed this objective before"
            } else {
                dataManager.saveStoryIfNotExists(objectiveInfo!, completed: true)
                gameRewardDetails.text = "You completed this objective for the first time and was rewarded a reverse"
                dataManager.setReverses(dataManager.getReverses()+1)
            }
            
        } else {
            finalScoreLabel.text! = "Failed. You did not complete the objective"
        }
        if dataManager.getStoryCompletedForName(objectiveInfo!) {
            
        }
        if let info = self.objectiveInfo {
            storyInfoText.text = info
        }
    }
    
    func freeModeEndView () {
        storyInfoText.hidden = true
        finalScoreLabel.text! = "You scored \(finalScore)"
        if (dataManager.shouldRewardAndAddReverse(self.finalScore)) && !reverseUsed {
            self.gameRewardDetails.text = "You earned a reverse!"
        }
        dataManager.addScore(finalScore, date: NSDate())
        reverseButtonOut.setTitle("Reverse (\(dataManager.getReverses()))", forState: UIControlState.Normal)
    }

    @IBAction func playAgainButton(sender: AnyObject) {
        newGame(0)
        reverseUsed = false
        uiUpdate()
    }
    
    @IBAction func shareButton(sender: AnyObject) {
        displayShareSheet()
    }

    @IBAction func reverseSteps(sender: AnyObject) {
        var stepsBack = 5
        if finalScore < 5 {
            stepsBack = finalScore
        }
        reverseUsed = true
        newGame(finalScore - stepsBack)
        dataManager.setReverses(dataManager.getReverses()-1)
        uiUpdate()
    }
    
    func displayShareSheet () {
        let sheet = UIAlertController(title: "Share", message: "Pick the social network you'd like to share on", preferredStyle: UIAlertControllerStyle.ActionSheet)
        let facebookButton = UIAlertAction(title: "Facebook", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            self.shareToSocial(SLServiceTypeFacebook)
        })
        sheet.addAction(facebookButton)
        let twitterButton = UIAlertAction(title: "Twitter", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            self.shareToSocial(SLServiceTypeTwitter)
        })
        sheet.addAction(twitterButton)
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        sheet.addAction(cancel)
        sheet.popoverPresentationController?.sourceView = self.view
        self.presentViewController(sheet, animated: true, completion: nil)
    }
    
    func shareToSocial(service: String) {
        let share: SLComposeViewController = SLComposeViewController(forServiceType: service)
        share.setInitialText("I just scored \(finalScore) in uTripped.\nWhat's your score?")
        share.setEditing(false, animated: false)
        self.presentViewController(share, animated: true, completion: nil)
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .Landscape
        } else {
            return .Landscape
        }
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewWillDisappear(animated: Bool) {
        scene.view?.paused = true
    }
}
