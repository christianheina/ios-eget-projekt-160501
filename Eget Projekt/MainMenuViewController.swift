//
//  MainMenuViewController.swift
//  Eget Projekt
//
//  Created by Christian on 2016-04-24.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation

class MainMenuViewController: UIViewController {
    var dataManager = DataManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "FreeMode" {
            let game = segue.destinationViewController as! GameViewController
            game.floor = "floor\(1 + arc4random()%self.dataManager.nBackgroundImages)"
        }
    }
    

}
