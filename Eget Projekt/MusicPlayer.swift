//
//  MusicPlayer.swift
//  Eget Projekt
//
//  Created by Christian on 2016-04-24.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

import UIKit
import AVFoundation

class MusicPlayer: NSObject, AVAudioPlayerDelegate {
    var player:AVAudioPlayer!
    var dataManager = DataManager()
    
    func playBackgroundSound() {
        do {
            try player = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("theme", ofType: "mp3")!))
        } catch let error as NSError {
            print(error)
        }
        player.numberOfLoops = -1
        player.volume = dataManager.getVolume()
        player.delegate = self
        player.prepareToPlay()
        player.play()
    }
    
    func volume(volume:Float) {
        player.volume = volume
    }
    
    func stopBackgroundSound() {
        player.stop()
    }
}
