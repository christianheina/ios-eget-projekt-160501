//
//  Score+CoreDataProperties.swift
//  Eget Projekt
//
//  Created by Christian on 2016-04-23.
//  Copyright © 2016 Christian Heina. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Score {

    @NSManaged var date: NSDate?
    @NSManaged var score: NSNumber?

}
