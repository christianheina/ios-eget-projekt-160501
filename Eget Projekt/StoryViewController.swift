//
//  StoryViewController.swift
//  Eget Projekt
//
//  Created by Christian on 2016-04-24.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

import UIKit

class StoryViewController: UIViewController {
    var objectiveInfo: [String] = []
    var objectiveButton: [UIButton] = []
    var dataManager = DataManager()
    var slowSpeed = 4.0

    @IBOutlet weak var firstObjectiveButton: UIButton!
    @IBOutlet weak var secondObjectiveButton: UIButton!
    @IBOutlet weak var thirdObjectiveButton: UIButton!
    @IBOutlet weak var fourthObjectiveButton: UIButton!
    @IBOutlet weak var fifthObjectiveButton: UIButton!
    @IBOutlet weak var sixthObjectiveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        objectiveInfo = [
            "Goal: 10\nLives: 3\nSpeed: Slow",
            "Goal: 20\nLives: 3\nSpeed: Slow",
            "Goal: 20\nLives: 1\nSpeed: Slow",
            "Goal: 10\nLives: 3\nSpeed: Normal",
            "Goal: 20\nLives: 3\nSpeed: Normal",
            "Goal: 20\nLives: 1\nSpeed: Normal"
        ]
        objectiveButton = [
            firstObjectiveButton,
            secondObjectiveButton,
            thirdObjectiveButton,
            fourthObjectiveButton,
            fifthObjectiveButton,
            sixthObjectiveButton
        ]
        for i in 0..<objectiveInfo.count {
            if dataManager.getStoryCompletedForName(objectiveInfo[i]) {
                let image = UIImageView(image: UIImage(named: "marker"))
                objectiveButton[i].addSubview(image)
                image.center = CGPointMake(objectiveButton[i].frame.width, objectiveButton[i].frame.height)
                image.alpha = 0.8
            }
        }
    }
    
    @IBAction func firstLevelButton(sender: AnyObject) {
        showGameView(10, lives: 3, dropSpeed: slowSpeed, info: objectiveInfo[0], floor: "floor1")
    }

    @IBAction func secondLevelButton(sender: AnyObject) {
        showGameView(20, lives: 3, dropSpeed: slowSpeed, info: objectiveInfo[1], floor: "floor2")
    }
    
    @IBAction func thirdLevelButton(sender: AnyObject) {
        showGameView(20, lives: nil, dropSpeed: slowSpeed, info: objectiveInfo[2], floor: "floor3")
    }
    
    @IBAction func fourthLevelButton(sender: AnyObject) {
        showGameView(10, lives: 3, dropSpeed: nil, info: objectiveInfo[3], floor: "floor4")
    }
    
    @IBAction func fifthLevelButton(sender: AnyObject) {
        showGameView(20, lives: 3, dropSpeed: nil, info: objectiveInfo[4], floor: "floor5")
    }
    
    @IBAction func sixthLevelButton(sender: AnyObject) {
        showGameView(20, lives: nil, dropSpeed: nil, info: objectiveInfo[5], floor: "floor6")
    }
    
    func showGameView(goal: Int, lives: Int?, dropSpeed: Double?, info: String, floor: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let game = storyboard.instantiateViewControllerWithIdentifier("GameView") as! GameViewController
        game.goal = goal
        if let lives = lives {
            game.lives = lives
        }
        if let dropSpeed = dropSpeed {
            game.baseDropInterval = dropSpeed
        }
        game.objectiveInfo = info
        game.floor = floor
        self.presentViewController(game, animated: true, completion: nil)
    }

}
