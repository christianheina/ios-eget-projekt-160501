//
//  TabViewController.swift
//  Eget Projekt
//
//  Created by Christian on 2016-04-25.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

import UIKit

class TabViewController: UITabBarController {
    var nextIndex = 0
    var maxIndex = 3

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.hidden = true
        NSTimer.scheduledTimerWithTimeInterval(6, target: self, selector: Selector("cycleThrough"), userInfo: nil, repeats: true)
    }
    
    func cycleThrough() {
        nextIndex += 1
        if nextIndex > maxIndex {
            nextIndex = 0
        }
        let toView = self.viewControllers![nextIndex]
        let viewSize = self.view.frame
        toView.view.frame = CGRectMake(viewSize.size.width, viewSize.origin.y, viewSize.size.width, viewSize.size.height)
        self.view.addSubview(toView.view)
        
        UIView.animateWithDuration(0.8, animations: {
                toView.view.frame = CGRectMake(0, viewSize.origin.y, viewSize.size.width, viewSize.size.height)
            })
    }

}
